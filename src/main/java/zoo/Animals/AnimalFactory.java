package zoo.Animals;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Optional;

import zoo.Exceptions.AnimalFactoryException;
public class AnimalFactory {

    private static final String ILLEGAL_ACCESS_EXCEPTION_MESSAGE = "Application tried to reflectively create" +
            " an instance, but the currently executing method does not have access to the definition of the" +
            " specified class, field, method or constructor.";
    private static final String INSTANTIATION_EXCEPTION_MESSAGE ="Application tried to reflectively create an" +
            " instance of a class using the newInstance method in class Class, but the class object cannot be instantiated.";
    private static final String NO_SUCH_METHOD_EXCEPTION_MESSAGE = "Reflective method- declared constructor of animal" +
            " class couldn't be found.";
    private static final String INVOCATION_TARGET_EXCEPTION_MESSAGE = "An exception occurred within the declared " +
            "constructor of the animal class";
    public static final String CLASS_NOT_FOUND_EXCEPTION_MESSAGE = "Application tried to load in a class through its string" +
            " name, but no definition for the class with the specified name could be found.";
    public static final Logger LOGGER = LoggerFactory.getLogger(AnimalFactory.class);

    private HashMap<String, Class> animalClassesHashMap;

    public AnimalFactory(){
        this.animalClassesHashMap = new HashMap<String, Class>();
    }

    public Optional<Animal> generateAnimalByClassPath(String classPath) throws AnimalFactoryException {
        LOGGER.info(String.format("Started generating animal by path: '%s'", classPath));
        Animal animal;
        Optional<Animal> optionalAnimal = Optional.empty();
        try {
            Optional<Class> optionalAnimalClass = getAnimalClassByPath(classPath);
            if(optionalAnimalClass.isPresent()) {
                Class animalClass = optionalAnimalClass.get();
                animal =  (Animal) animalClass.getDeclaredConstructor().newInstance();
                optionalAnimal = Optional.ofNullable(animal);
            }
        }
        catch (IllegalAccessException e) {
            throw new AnimalFactoryException(ILLEGAL_ACCESS_EXCEPTION_MESSAGE);
        } catch (InstantiationException e) {
            throw new AnimalFactoryException(INSTANTIATION_EXCEPTION_MESSAGE);
        }  catch (NoSuchMethodException e) {
            throw new AnimalFactoryException(NO_SUCH_METHOD_EXCEPTION_MESSAGE);
        } catch (InvocationTargetException e) {
            throw new AnimalFactoryException(INVOCATION_TARGET_EXCEPTION_MESSAGE);
        }
        LOGGER.info(String.format("Finished generating animal by path: '%s'", classPath));
        return optionalAnimal;
    }


    private Optional<Class> getAnimalClassByPath(String classPath) throws AnimalFactoryException {
        LOGGER.info(String.format("Started getting animal class by path: '%s'", classPath));

        Class animalClass = this.animalClassesHashMap.get(classPath);
        if(animalClass == null){
            try {
                animalClass = Class.forName(classPath);
            } catch (ClassNotFoundException e) {
                throw new AnimalFactoryException(CLASS_NOT_FOUND_EXCEPTION_MESSAGE);
            }
            this.animalClassesHashMap.put(classPath,animalClass);
        }
        LOGGER.info(String.format("Finished getting animal class by path: '%s'", classPath));
        return Optional.ofNullable(animalClass);
    }

}
