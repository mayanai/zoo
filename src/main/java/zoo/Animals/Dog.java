package zoo.Animals;

public class Dog extends Animal {

    private static final String DOG_NAME = "Dog";
    private static final String DOG_SOUND = "Woof";

    public Dog(){
        this.name = DOG_NAME;
        this.sound = DOG_SOUND;
    }
}
