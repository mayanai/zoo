package zoo.Animals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zoo.Exceptions.AnimalFactoryException;
import zoo.Exceptions.FileHandlerException;
import zoo.Exceptions.IncorrectFileExtensionException;
import zoo.PropertiesManager;
import zoo.Utils.FileHandlerUtils;

import java.util.ArrayList;
import java.util.Optional;


public class AnimalManager {

    private static final String ANIMAL_PROPERTIES_FILE_PATH = "src/main/resources/AnimalProperties.properties";
    public static final Logger LOGGER = LoggerFactory.getLogger(AnimalManager.class);

    public static void executeZoo(String filePath){
        LOGGER.info(String.format("Started executing zoo with path: '%s'", filePath));
        try {
            PropertiesManager animalsPropertiesManager = new PropertiesManager(ANIMAL_PROPERTIES_FILE_PATH);
            ArrayList<String> animalNamesList = FileHandlerUtils.generateStringListFromFile(filePath);
            AnimalFactory animalFactory = new AnimalFactory();

            animalNamesList.forEach( (animalName) -> {
                try {
                    printAnimalBy(animalName, animalsPropertiesManager, animalFactory);
                } catch (AnimalFactoryException e) {
                    LOGGER.error(e.getMessage(),e);
                }
            });
        }
        catch (IncorrectFileExtensionException | FileHandlerException e){
            LOGGER.error(e.getMessage(), e);
        }
        LOGGER.info("Finished executing zoo");
    }

    public static void printAnimalBy(String animalName, PropertiesManager animalsPropertiesManager, AnimalFactory animalFactory)
            throws AnimalFactoryException {
        LOGGER.info(String.format("Started printing animal called: '%s'", animalName));
        String animalTypeClassPath = animalsPropertiesManager.getValue(animalName);
        if(animalTypeClassPath == null){
            LOGGER.error(String.format("'%s' is not a valid animal name. ",animalName));
        }
        else{
            System.out.println(animalName);
            Optional<Animal> optionalAnimal = animalFactory.generateAnimalByClassPath(animalTypeClassPath);
            printOptionalAnimalSound(optionalAnimal);
        }
        LOGGER.info(String.format("Finished printing animal '%s'", animalName));
    }

    public static void printOptionalAnimalSound(Optional<Animal> optionalAnimal) {
        LOGGER.info(String.format("Started printing sound of optional animal '%s'", optionalAnimal));
        if(optionalAnimal.isPresent()) {
            optionalAnimal.get().printSound();
        }
        else{
            LOGGER.error("Animal not present");
        }
        LOGGER.info("Finished printing animal sound");
    }
}
