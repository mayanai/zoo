package zoo.Animals;

public class Cat extends Animal {
    private static final String CAT_NAME = "Cat";
    private static final String CAT_SOUND = "Meow";

    public Cat(){
        this.name = CAT_NAME;
        this.sound = CAT_SOUND;
    }
}
