package zoo.Animals;

public class Duck extends Animal {

    private static final String DUCK_NAME = "Duck";
    private static final String DUCK_SOUND = "Ga ga";

    public Duck(){
        this.name = DUCK_NAME;
        this.sound = DUCK_SOUND;
    }
}

