package zoo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zoo.Animals.AnimalManager;
import zoo.Exceptions.*;

public class Main {

    private static final String WRONG_ARGUMENTS_NUMBER_EXCEPTION_MESSAGE = "Exactly 1 argument required!";
    public static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if(args.length!= 1) {
            LOGGER.error("Program was given more/less than one argument");
            throw new WrongArgumentsNumberException(WRONG_ARGUMENTS_NUMBER_EXCEPTION_MESSAGE);
        }
        String filePath = args[0];
        AnimalManager.executeZoo(filePath);
    }
}
