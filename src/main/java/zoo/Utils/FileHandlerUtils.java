package zoo.Utils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zoo.Exceptions.*;

public class FileHandlerUtils {

    private static final String ONE_OR_MORE_SPACES_DELIMITER_REGEX = " +";
    private static final String TEXT_FILE_EXTENSION = "txt";
    private static final String PROPERTIES_FILE_EXTENSION = "properties";
    private static final String PROPERTIES_FILE_NOT_FOUND_EXCEPTION_MESSAGE = "An error occurred, properties file not found.";
    private static final String PROPERTIES_IO_EXCEPTION_MESSAGE = "Could not load properties from the given file.";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileHandlerUtils.class);


    public static ArrayList<String> generateStringListFromFile(String filePath) throws IncorrectFileExtensionException,
            FileHandlerException {
        LOGGER.info(String.format("Started generating string list from file: '%s'", filePath));
        if(isNotTextFile(filePath)){
            throw new IncorrectFileExtensionException(String.format("file '%s' requested to open for reading is not a .txt file",
                    filePath));
        }
        ArrayList<String> stringList = new ArrayList<String>();
        try {
            File file = new File(filePath);
            Scanner fileScanner = new Scanner(file);
            LOGGER.info("opened file scanner");
            fileScanner.useDelimiter(ONE_OR_MORE_SPACES_DELIMITER_REGEX);
            String item;
            while (fileScanner.hasNext()) {
                item = fileScanner.next();
                stringList.add(item);
            }
            fileScanner.close();
            LOGGER.info("closed file scanner");
        }
        catch (FileNotFoundException e) {
            throw new FileHandlerException(String.format("An error occurred, file '%s' not found.", filePath));
        }
        LOGGER.info(String.format("Finished generating string list from file: '%s'", filePath));
        return stringList;
    }

    public static Properties loadPropertiesFile(String filePath) throws IncorrectFileExtensionException,
            FileHandlerException{
        LOGGER.info(String.format("Started loading properties file from: '%s'", filePath));
        if(isNotPropertiesFile(filePath)){
            LOGGER.error(String.format("'%s' is not a properties file", filePath));
            throw new IncorrectFileExtensionException(String.format("file '%s' requested to open for reading is" +
                    " not a .properties file", filePath));
        }
        Properties propertiesFromFile =new Properties();
        FileInputStream propertiesFile;
        try {
            propertiesFile = new FileInputStream(filePath);
            propertiesFromFile.load(propertiesFile);
        }
        catch (FileNotFoundException e) {
            throw new FileHandlerException(PROPERTIES_FILE_NOT_FOUND_EXCEPTION_MESSAGE);
        }
        catch (IOException e) {
            throw new FileHandlerException(PROPERTIES_IO_EXCEPTION_MESSAGE);
        }
        LOGGER.info(String.format("Finished loading properties file from: '%s'", filePath));
        return propertiesFromFile;
    }

    public static boolean isNotTextFile(String filePath){
        return !(FilenameUtils.getExtension(filePath).equals(TEXT_FILE_EXTENSION));
    }

    public static boolean isNotPropertiesFile(String filePath){
        return !(FilenameUtils.getExtension(filePath).equals(PROPERTIES_FILE_EXTENSION));
    }
}
