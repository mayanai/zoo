package zoo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zoo.Exceptions.FileHandlerException;
import zoo.Exceptions.IncorrectFileExtensionException;
import zoo.Utils.FileHandlerUtils;

import java.util.Properties;


public class PropertiesManager {

    private Properties properties;
    public static final Logger LOGGER = LoggerFactory.getLogger(PropertiesManager.class);

    public PropertiesManager(String propertiesFilePath) throws IncorrectFileExtensionException, FileHandlerException {
        LOGGER.info("Creating new properties manager");
        this.properties = FileHandlerUtils.loadPropertiesFile(propertiesFilePath);
    }

    public String getValue(String key){
       return this.properties.getProperty(key);
    }

}
