package zoo.Exceptions;

public class IncorrectFileExtensionException extends Exception {
    public IncorrectFileExtensionException(String errorMessage) {
        super(errorMessage);
    }
}
