package zoo.Exceptions;

public class FileHandlerException extends Exception {
    public FileHandlerException(String errorMessage) {
        super(errorMessage);
    }
}