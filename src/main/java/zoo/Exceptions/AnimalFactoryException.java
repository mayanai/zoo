package zoo.Exceptions;

public class AnimalFactoryException extends Exception {
    public AnimalFactoryException(String errorMessage) {
        super(errorMessage);
    }
}
