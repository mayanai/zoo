package zoo.Exceptions;

public class WrongArgumentsNumberException extends RuntimeException {
    public WrongArgumentsNumberException(String errorMessage) {
        super(errorMessage);
    }
}
